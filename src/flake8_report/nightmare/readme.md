# Overview 

This is a Nightmare Coding Sample for generating all the flake8 error codes
that I can for testing. Note that many of the total possible error codes
simply can not be generated with the same version of Python. Some only pertain 
to Python 2 and some to Python 3.9

