flake8-report is a format plugin for flake8.

This package will register 3 flake8 format plug-ins:

* `--format report   ` Formatted text report
* `--format checklist` Markdown checklists
* `--format wiki     ` Markdown doc suitable for Gitlab wiki pages

The report output is a big more human-readable with some flexibility of how the
report listing is sorted/grouped.

The checklists are in markdown and can be pasted into the body of any markdown doc -
in my case, into Gitlab issues.

The wiki content is also in markdown with a bias toward Gitlab wiki pages.

# Installation

Install the package with pip:

```back
# Presumes you already have `flake8` installed
pip install flake8-report
```

# Usage

## Configuration

This plugin present two configuration options:

* `report-style` : One of ['none', 'group']
* `report-group` : One of ['none', 'code', 'path']

These options may be specified on the commandline, or in the config file.

### Command-line
```commandline
flake8 --format report --report-group code
``` 

### Config File

The config may be added along with the other flake8 options in the flake8-supported
config files.

```text
[flake8]
report-style = group
report-group = path
```

## Execution

### Simple

```commandline
flake8 --format report
flake8 --format checklist
```

### With Options

```commandline
flake8 --format report --report-group code src/
```

