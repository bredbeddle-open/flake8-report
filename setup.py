#!/usr/bin/env python
"""Packaging logic for Flake8-report."""

import sys
import os
import setuptools

sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'src'))
from flake8_report.version import __version__  # noqa

# See setup.cfg for metadata
setuptools.setup(
    version=__version__,
)
